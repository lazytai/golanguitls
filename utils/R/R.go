package R

import (
	"github.com/gin-gonic/gin"
)

func Success(c *gin.Context, data interface{}, message string) {
	var response = make(map[string]interface{})
	response["data"] = data
	response["message"] = message
	response["code"] = 200
	c.JSON(200, response)
}
/*func Success(c *gin.Context, data interface{}) {
	var response = make(map[string]interface{})
	response["data"] = data
	response["message"] = ""
	response["code"] = 200
	c.JSON(200, response)
}*/

func Fail(c *gin.Context, code int, data interface{}, message string) {
	var response = make(map[string]interface{})
	response["data"] = data
	response["message"] = message
	if code == 0 {
		code = 500
	}
	response["code"] = code
	c.JSON(code, response)
}
