package utils

import "github.com/google/uuid"
import "strings"
import "github.com/sony/sonyflake"
import "crypto/rand"
import "fmt"

/* 32位 */
func Uuid() string {
	var str = uuid.NewString()
	return str
}

func Uuidno_() string {
	var str2 = strings.ReplaceAll(Uuid(), "-", "")
	return str2
}

func UuidSnow() uint64 {
	factory := sonyflake.NewSonyflake(sonyflake.Settings{})
	uint64var, _ := factory.NextID()
	return uint64var
}

//生成特定的随机数
func RandomCode(n int) string {
	b := make([]byte, n/2)
	if _, err := rand.Read(b); err != nil {
		panic(err)
	}
	s := fmt.Sprintf("%X", b)
	return s
}

//默认8位 短的
func UuidShort() string {
	return RandomCode(8)
}
