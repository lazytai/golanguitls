package utils

import (
	"github.com/gin-gonic/gin"
	"github.com/lazyTai/golangutils/router"
)

func SetUpServer() *gin.Engine {
	srv := gin.Default()



	srv.GET("/", func(c *gin.Context) {
		c.JSON(200, "OK")
	})

	srv.GET("/homerouterfail", router.HomeRouterFail)

	homeRouter := router.HomeRouter{}
	homeRouter.SetUp(srv)



	srv.Run(":9999")
	return srv
}
