module github.com/lazyTai/golangutils

go 1.16

require (
	github.com/astaxie/beego v1.12.3
	github.com/gin-gonic/gin v1.7.4
	github.com/google/uuid v1.3.0
	github.com/sony/sonyflake v1.0.0
)
