package router

import "github.com/gin-gonic/gin"

type BaseController struct {
	c *gin.Context
}

func init() {

}

func (this *BaseController) getParams() map[string]string {
	params := map[string]string{}
	this.c.BindQuery(&params)
	return params
}

func (this *BaseController) getBody() map[string]string {
	params := map[string]string{}
	this.c.BindJSON(&params)
	return params
}
func (this *BaseController) getBodyWithStruct(_struct interface{}) {
	this.c.BindJSON(&_struct)
}

func (this *BaseController) Success(data interface{}, message string) {
	var response = make(map[string]interface{})
	response["data"] = data
	response["message"] = message
	response["code"] = 200
	this.c.JSON(200, response)
}

func (this *BaseController) Fail(code int, data interface{}, message string) {
	var response = make(map[string]interface{})
	response["data"] = data
	response["message"] = message
	if code == 0 {
		code = 500
	}
	response["code"] = code
	this.c.JSON(code, response)
}
