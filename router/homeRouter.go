package router

import (
	"github.com/gin-gonic/gin"
	"github.com/lazyTai/golangutils/utils/R"
)

// type HomeController struct{
// 	gin.IRouter
// }

type HomeRouter struct {
	BaseController
}
type Person struct {
	Name string `json:"name"`
}

func (this *HomeRouter) SetUp(engine *gin.Engine) {
	engine.Use(func(context *gin.Context) {
		this.c=context;
	})


	engine.GET("/helloWorld", this.HelloWorld)
	engine.POST("/post", this.post)
	engine.POST("/post02", this.post02)
}
func (this *HomeRouter) HelloWorld(c *gin.Context) {
	params := this.getParams()
	this.Success(params, "")
}
func (this *HomeRouter) post(c *gin.Context) {
	body := this.getBody()
	this.Success(body, "")
}

func (this *HomeRouter) post02(c *gin.Context) {
	person := Person{}
	this.getBodyWithStruct(&person)
	this.Success(person, "")
}

func HomeRouterTest(c *gin.Context) {
	_map := map[string]string{
		"hello": "world",
	}
	R.Success(c, _map, "你好")
}
func HomeRouterFail(c *gin.Context) {
	_map := map[string]string{
		"hello": "world",
	}
	R.Fail(c, 500, _map, "失败了")
}
